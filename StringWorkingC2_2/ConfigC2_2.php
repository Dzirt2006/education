<?php
/**
 * Created by PhpStorm.
 * User: dzirt
 * Date: 12/11/2018
 * Time: 7:32 PM
 */

function SubString($String,$maxStr){
    if ($String && $maxStr){
        if (strlen($String)>$maxStr){
            $textRes=substr($String,0,$maxStr);
            $textRes=trim($textRes);
            return $textRes.'...';
        }else
            return $String;
    }else
        return 'Wrong data.';
}