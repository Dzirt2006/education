<?php
/**
 *Задача написать функцию, автоматизирующиме следующие мыслительные процессы в голове члена приемной комиссии:
" Мы учитываем аттестат: во первых мы смотрим на:
 * физику, химию и геометрию - хотя бы по двум из них должны быть пятерки,
 * во вторых не берем, если у человека тройка по русскому или литературе,
 * и в тритьих по информатике - должна быть обязательно пятерка"

 * Подсказка:
все предметы в аттестате (входные данные функции) являются целыими числами между 2 и 5
```php
function canStudy (integer $fizika, integer $himia, integer $fgeometrya, integer $russkii, integer $informatika) {

// здесь ваш код
return true; // если подходит
return false // если не подходит
}
```
 */
include 'ConfigC1_2.php';

$fizika=5;
$himia=5;
$fgeometrya=2;
$russkii=4;
$informatika=5;
$literature=5;


var_dump(canStudy($fizika,$himia,$fgeometrya,$russkii,$informatika,$literature));